import { createIconSet } from "@expo/vector-icons";
const glyphMap = { hero: "1", human: "h", droid: "2", starship: "g" };

// http://www.dafont.com/galaxy-far-far-away.font
export default createIconSet(glyphMap, "galaxyfaraway");
