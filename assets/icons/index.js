export default {
  bobaFett: require("./boba-fett.png"),
  c3p0: require("./c3p0.png"),
  r2d2: require("./app.png"),
  darthVader: require("./darth-vader.png"),
  emperor: require("./emperor.png"),
  stormTrooper: require("./loading.png"),
  hanSolo: require("./han-solo.png"),
  human: require("./human.png"),
  leah: require("./leah.png"),
  luke: require("./luke.png"),
  obiWan: require("./obi-wan.png"),
  yoda: require("./yoda.png"),
  starship: require("./starship.png"),
  droid: require("./droid.png"),
  bb8: require("./bb8.png"),
  r2d2Icon: require("./r2d2.png")
};
