import React from "react";
import { StyleSheet, ScrollView } from "react-native";
import Planets from "./planets";
import Characters from "./characters";

class HomeScreen extends React.Component {
  render() {
    return (
      <ScrollView
        style={styles.container}
        indicatorStyle={"white"}
        scrollEventThrottle={200}
      >
        <Characters />
        <Planets />
      </ScrollView>
    );
  }
}

export default HomeScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
