import React from "react";
import { LinearGradient } from "expo";
import { StyleSheet, View, Image, Dimensions } from "react-native";
import { Text } from "react-native-elements";
import Carousel from "react-native-snap-carousel";
import uiTheme from "../Theme/uiTheme";
import { gql, graphql } from "react-apollo";
import apolloLoadingHoc from "./apolloLoadingHoc";
import CardCarousel from "../Components/card_carousel.js";
import { BodyHeaderText } from "../Components/generic.js";

class Heroes extends React.Component {
  getSlides = entries => {
    return entries.map(entry => (
      <CardCarousel.Card key={entry.id}>
        <Image
          source={{
            uri: entry.photoUrl || "http://i.imgur.com/s7jPOXu.jpg"
          }}
          style={{
            flex: 3,
            alignSelf: "stretch",
            resizeMode: "cover",
            backgroundColor: "black"
          }}
        />
        <LinearGradient
          colors={[
            "transparent",
            "rgba(72, 71, 75, 0.1)",
            "rgba(72, 71, 75, 0.3)",
            "rgba(72, 71, 75, 0.7)",
            "rgba(72, 71, 75, 1)"
          ]}
          style={{
            padding: 10,
            position: "absolute",
            justifyContent: "flex-end",
            bottom: 0,
            right: 0,
            left: 0,
            height: 150
          }}
        >
          <Text
            h4
            style={{
              fontWeight: 0.4,
              color: "#F6F6F7",
              textShadowColor: "rgba(72, 71, 75, 0.5)",
              textShadowOffset: { width: 1, height: 1 }
            }}
          >
            {entry.name}
          </Text>
          {entry.species &&
            <Text
              h5
              style={{
                color: "#AAA6A7",
                textShadowColor: "rgba(72, 71, 75, 0.5)",
                textShadowOffset: { width: 1, height: 1 }
              }}
            >
              {entry.species.name}
            </Text>}
        </LinearGradient>
      </CardCarousel.Card>
    ));
  };
  render() {
    const { data } = this.props;
    return (
      <View style={{ height: 500 }}>
        <BodyHeaderText>
          Characters
        </BodyHeaderText>
        <CardCarousel>
          {this.getSlides(data.viewer.allPeople.edges.map(char => char.node))}
        </CardCarousel>
      </View>
    );
  }
}

const allCharacters = gql`
  query charactersCharactersQuery {
    viewer {
      allPeople(first: 10) {
        edges {
          node {
            id,
            name,
            photoUrl
            species {
              id
              name
            }
          }
        }
      }
    }
  }
`;

export default graphql(allCharacters, {})(apolloLoadingHoc(Heroes));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center"
  }
});
