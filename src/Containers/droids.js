import React, { Component } from "react";
import { View, Image } from "react-native";
import { BodyText } from "./characters";
import icons from "../../assets/icons";

class Droids extends Component {
  render() {
    return <View><BodyText h3>Droids</BodyText></View>;
  }
}

export default Droids;
