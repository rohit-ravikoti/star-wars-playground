import React from "react";
import { View, Text, Image, AsyncStorage, ScrollView } from "react-native";
import { Button } from "react-native-elements";
import uiTheme from "../Theme/uiTheme";
import Loading from "../Components/loading";
import client from "../networking";

export default Component => {
  return ({ data, ...rest }) => {
    if (data.loading) {
      return <Loading>Loading...</Loading>;
    }
    if (data.error) {
      console.log("graphql error", data.error);
      if (data.error.message.includes("status 401")) {
        AsyncStorage.clear();
        client.resetStore();
      }
      return (
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: uiTheme.palette.bodyColor
          }}
        >
          <Image
            source={require("../../assets/icons/darth-vader.png")}
            style={{ height: 70, margin: 10 }}
            resizeMode="contain"
          />
          <Text
            style={{
              fontFamily: "open-sans",
              padding: 20,
              textAlign: "center",
              color: uiTheme.palette.bodyTextColor
            }}
          >
            {data.error.message.includes("Network error")
              ? `You are not connected to the internet!${"\n"}${"\n"}To make use of your Raizzz offers, we require internet access on your device.${"\n"}${"\n"}Please try again when you are connected.`
              : "Sorry, there was an error."}
          </Text>
          <Button
            backgroundColor={uiTheme.palette.activeColor}
            title="Try Again"
            fontFamily="open-sans"
            borderRadius={3}
            onPress={() => data.refetch()}
          />
        </View>
      );
    }
    return <Component data={data} {...rest} />;
  };
};
