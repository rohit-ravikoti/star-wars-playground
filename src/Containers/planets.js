import React from "react";
import { StyleSheet, View, Image, Dimensions } from "react-native";
import { Text } from "react-native-elements";
import Carousel from "react-native-snap-carousel";
import uiTheme from "../Theme/uiTheme";
import { gql, graphql } from "react-apollo";
import apolloLoadingHoc from "./apolloLoadingHoc";
import CardCarousel from "../Components/card_carousel.js";
import { BodyHeaderText } from "../Components/generic.js";

class Heroes extends React.Component {
  getSlides = entries => {
    return entries.map(entry => (
      <CardCarousel.Card key={entry.id}>
        <Image
          source={{
            uri: entry.photoUrl || "http://i.imgur.com/IABm2pM.jpg"
          }}
          style={{
            flex: 3,
            alignSelf: "stretch",
            resizeMode: "cover",
            backgroundColor: "black"
          }}
        />
      </CardCarousel.Card>
    ));
  };
  render() {
    const { data } = this.props;
    return (
      <View style={{ height: 500 }}>
        <BodyHeaderText>
          Planets
        </BodyHeaderText>
        <CardCarousel>
          {this.getSlides(data.viewer.allPlanets.edges.map(char => char.node))}
        </CardCarousel>
      </View>
    );
  }
}

const allPlanets = gql`
  query PlanetsQuery {
    viewer {
      allPlanets(first: 10) {
        edges {
          node {
            id,
            name,
            photoUrl
          }
        }
      }
    }
  }
`;

export default graphql(allPlanets, {})(apolloLoadingHoc(Heroes));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
});
