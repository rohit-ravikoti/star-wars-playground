import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import icons from "../../assets/icons";
import { BodyText } from "./characters";

class Starships extends Component {
  render() {
    return <View><BodyText h3>Starships</BodyText></View>;
  }
}

export default Starships;
