import React from "react";
import { View, Text } from "react-native";
import styled from "styled-components/native";
import { TabNavigator, TabBarTop } from "react-navigation";
import HomeScreen from "../Containers/HomeScreen";
import Starships from "../Containers/starships";
import Humans from "../Containers/humans";
import Droids from "../Containers/droids";
import Loading from "../Components/loading";
import uiTheme from "../Theme/uiTheme.js";
import icons from "../../assets/icons";

const Image = styled.Image`
  resize-mode: contain;
`;

export default TabNavigator(
  {
    Play: {
      screen: () => <View style={{ flex: 1 }}><Loading /></View>,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Image source={icons.bb8} style={{ width: 20, height: 20 }} />
        )
      }
    },
    Home: {
      screen: HomeScreen,
      navigationOptions: {
        tabBarIcon: ({ tintColor }) => (
          <Image
            source={icons.r2d2Icon}
            style={{ marginTop: 5, width: 60, height: 60 }}
          />
        )
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: uiTheme.palette.activeColor,
      inactiveTintColor: uiTheme.palette.inactiveColor,
      showIcon: true,
      showLabel: false,
      style: {
        paddingTop: uiTheme.statusBarSpacing,
        backgroundColor: uiTheme.palette.primaryColor
      },
      indicatorStyle: {
        backgroundColor: uiTheme.palette.activeColor
      }
    },
    initialRouteName: "Home",
    animationEnabled: true,
    lazy: true,
    tabBarComponent: TabBarTop,
    tabBarPosition: "top"
  }
);
