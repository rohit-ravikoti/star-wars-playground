import { ApolloClient, createNetworkInterface } from "react-apollo";
import { AsyncStorage } from "react-native";
var config = require("../config.json");

// Apollo
const networkInterface = createNetworkInterface({
  uri: config.apiEndpoint
});

networkInterface.use([
  {
    applyMiddleware(req, next) {
      if (!req.options.headers) {
        req.options.headers = {}; // Create the header object if needed.
      }
      AsyncStorage.getItem("currentUser", (err, res) => {
        if (err) {
          alert("There was an authentication error");
          next();
        } else {
          const currentUser = JSON.parse(res);
          const token = currentUser && currentUser.token;
          if (token) {
            req.options.headers.authorization = `Bearer ${token}`;
          }
          next();
        }
      });
    }
  }
]);

export default new ApolloClient({ networkInterface });
