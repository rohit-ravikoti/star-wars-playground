import Expo from "expo";
import { Platform } from "react-native";

export default {
  statusBarSpacing: Expo.Constants.statusBarHeight,
  palette: {
    primaryColor: "#869f9d",
    activeColor: "white",
    inactiveColor: "#27799C",
    bodyColor: "#2E3B44",
    bodyTextColor: "#cccccc"
  }
};
