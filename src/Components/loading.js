import React, { Component } from "react";
import { Dimensions, View, Text, WebView } from "react-native";
import images from "../../assets/icons";
import uiTheme from "../Theme/uiTheme";
const { width: deviceWidth, height: deviceHeight } = Dimensions.get("window");

class Loading extends Component {
  render() {
    return (
      <View
        style={{
          position: "absolute",
          top: 0,
          right: 0,
          bottom: 0,
          left: 0,
          backgroundColor: uiTheme.palette.primaryColor
        }}
      >
        <WebView
          automaticallyAdjustContentInsets={true}
          style={{ flex: 1 }}
          source={{
            uri: "https://s.codepen.io/rravikot/debug/eWwzbj/vWARwnRQRjek"
          }}
          javaScriptEnabled={true}
          domStorageEnabled={true}
          // decelerationRate="normal"
          startInLoadingState={false}
          scalesPageToFit={true}
        />
      </View>
    );
  }
}

export default Loading;
