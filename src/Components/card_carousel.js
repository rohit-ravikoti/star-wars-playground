import React from "react";
import { View, Dimensions } from "react-native";
import { Text } from "react-native-elements";
import Carousel from "react-native-snap-carousel";
import uiTheme from "../Theme/uiTheme";
import styled from "styled-components/native";

const { width: viewportWidth, height: viewportHeight } = Dimensions.get(
  "window"
);

function wp(percentage) {
  const value = percentage * viewportWidth / 100;
  return Math.round(value);
}

export const slideHeight = viewportHeight * 0.6;
export const slideWidth = wp(75);
export const itemHorizontalMargin = wp(2);

export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

export const BodyText = styled(Text)`
  color: ${({ theme }) => theme.palette.bodyTextColor};
  margin: 10px;
`;

const CardCarousel = ({ children }) => {
  return (
    <Carousel
      sliderWidth={sliderWidth}
      itemWidth={itemWidth}
      inactiveSlideScale={0.94}
      inactiveSlideOpacity={0.6}
      enableMomentum={false}
      containerCustomStyle={{
        height: 500,
        paddingVertical: 10
      }}
      showsHorizontalScrollIndicator={false}
      snapOnAndroid={true}
      removeClippedSubviews={false}
    >
      {children}
    </Carousel>
  );
};

CardCarousel.Card = ({ children }) => (
  <View
    style={{
      height: slideHeight,
      width: itemWidth,
      backgroundColor: "transparent"
    }}
  >
    {children}
  </View>
);

export default CardCarousel;
