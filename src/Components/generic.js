import React from "react";
import styled from "styled-components/native";
import { BodyText } from "../Components/card_carousel.js";

export const BodyHeaderText = ({ children }) => (
  <BodyText h4>{children}</BodyText>
);
