import Expo from "expo";
import React from "react";
import { View, Image, Dimensions } from "react-native";
import { ThemeProvider } from "styled-components";
import { ApolloProvider } from "react-apollo";

import Loading from "./src/Components/loading";
import Root from "./src/Navigation/root";
import images from "./assets/icons";
import uiTheme from "./src/Theme/uiTheme";
import client from "./src/networking";

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === "string") {
      return Image.prefetch(image);
    } else {
      return Expo.Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => Expo.Font.loadAsync(font));
}

class App extends React.Component {
  state = {
    loaded: false
  };
  async componentDidMount() {
    await this._loadAssetsAsync();
    this.setState({ loaded: true });
  }
  _loadAssetsAsync = async () => {
    const imageAssets = cacheImages(
      Object.keys(images).map(img => images[img])
    );
    const fontAssets = cacheFonts([
      {
        galaxyfaraway: require("./assets/fonts/galaxyfaraway.ttf"),
        "open-sans": require("./assets/fonts/OpenSans-Regular.ttf")
      }
    ]);
    await Promise.all([...imageAssets, ...fontAssets]);
    return;
  };
  render() {
    return (
      <ApolloProvider client={client}>
        <ThemeProvider theme={uiTheme}>
          <View style={{ flex: 1, backgroundColor: uiTheme.palette.bodyColor }}>
            {this.state.loaded ? <Root /> : <Loading type="r2d2" />}
          </View>
        </ThemeProvider>
      </ApolloProvider>
    );
  }
}

Expo.registerRootComponent(App);
